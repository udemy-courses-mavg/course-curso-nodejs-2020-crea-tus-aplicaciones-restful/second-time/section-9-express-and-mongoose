const express = require('express')
const router = express.Router()
const { check, validationResult } = require('express-validator');
const Car = require('../models/car')
const auth = require('../middleware/auth')
const authorize = require('../middleware/role')
const Role = require('../helper/role')
const { Company } = require('../models/company')

// ------------------------------------------------------------ GET ------------------------------------------------------------
router.get('/', [auth, authorize([Role.Admin])], async (req, res) => {
  const cars = await Car
    .find()
    // GET Modelo de Datos Normalizado
    // .populate('company', 'name country')
  res.send(cars)
})

router.get('/:id', async (req, res) => {
  const car = await Car.findById(req.params.id)
  if(!car)
    return res.status(404).send('No se encontro un coche con ese ID')
  res.send(car)
})


// ------------------------------------------------------------ POST ------------------------------------------------------------ 
// POST Modelo de Datos Normalizado
/*
router.post('/', [
  // check('company').isLength({min: 2}),
  check('model').isLength({min: 2})
],
async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ errors: errors.array() });

  const car = new Car({
    company: req.body.company,
    model: req.body.model,
    year: req.body.year,
    sold: req.body.sold,
    price: req.body.price,
    extras: req.body.extras
  })

  const result = await car.save()
  res.status(201).send(result)
})
*/

// POST Modelo de Datos Embebido
router.post('/', [
  check('year').isLength({min: 2}),
  check('model').isLength({min: 2})
],
async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ errors: errors.array() });

  const company = await Company.findById(req.body.companyId)
  if(!company) 
    return res.status(400).send('No tenemos ese fabricante')

  const car = new Car({
    company: company,
    model: req.body.model,
    year: req.body.year,
    sold: req.body.sold,
    price: req.body.price,
    extras: req.body.extras
  })

  const result = await car.save()
  res.status(201).send(result)
})


// ------------------------------------------------------------ PUT ------------------------------------------------------------
router.put('/:id', [
  // check('company').isLength({min: 2}),
  check('model').isLength({min: 2})
],
async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ errors: errors.array() });

  const car = await Car.findByIdAndUpdate(req.params.id, {
    company: req.body.company,
    model: req.body.model,
    year: req.body.year,
    sold: req.body.sold,
    price: req.body.price,
    extras: req.body.extras
  },
  {
    new: true // Si es true, findByIdAndUpdate devuelve el documento actualizado en lugar del original https://mongoosejs.com/docs/api/model.html#model_Model.findByIdAndUpdate
  })

  if(!car)
    return res.status(404).send('No se encontro un coche con ese ID')
    
  res.sendStatus(204)
})


// ------------------------------------------------------------ DELETE ------------------------------------------------------------
router.delete('/:id', async (req, res) => {
  const car = await Car.findByIdAndDelete(req.params.id)

  if(!car){
    return res.status(404).send("El coche con ese ID no se encuentra registrado")
  }

  res.status(200).send("Coche borrado")
})


module.exports = router