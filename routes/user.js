const express = require('express')
const bcrypt = require('bcryptjs')
const User = require('../models/user')
const router = express.Router()
const { check, validationResult } = require('express-validator');

// ------------------------------------------------------------ GET ------------------------------------------------------------
router.get('/', async (req, res) => {
  const users = await User.find()
  res.send(users)
})

router.get('/:id', async (req, res) => {
  const user = await User.findById(req.params.id)
  if(!user)
    return res.status(404).send('No se encontro un usuario con ese ID')
  res.send(user)
})


// ------------------------------------------------------------ POST ------------------------------------------------------------ 
router.post('/', [
  check('name').isLength({min: 2}),
  check('email').isLength({min: 2}),
  check('password').isLength({min: 3})
],
async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ errors: errors.array() });

  let user = await User.findOne({email: req.body.email})
  if(user) 
    return res.status(400).send('Ese usuario ya existe')

  const salt = await bcrypt.genSalt(10)
  const hashPassword = await bcrypt.hash(req.body.password, salt)

  user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashPassword,
    isCustomer: false,
    isAdmin: req.body.isAdmin,
    role: req.body.role
  })

  const result = await user.save()

  const jwtToken = user.generateJWT()
  // const jwtToken = jwt.sign({_id: user._id, name: user.name}, 'password_clavesita_pequeñita')
  /*
  res.status(201).send({
    _id: user._id,
    name: user.name,
    email: user.email
  })
  */
  res.status(201).header('Authorization', jwtToken).send()
})


// ------------------------------------------------------------ PUT ------------------------------------------------------------
router.put('/:id', [
  check('name').isLength({min: 2}),
  check('email').isLength({min: 2})
],
async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ errors: errors.array() });

  const user = await User.findByIdAndUpdate(req.params.id, {
    name: req.body.name,
    email: req.body.email,
    isCustomer: req.body.isCustomer
  },
  {
    new: true
  })

  if(!user)
    return res.status(404).send('No se encontro un usuario con ese ID')
    
  res.sendStatus(204)
})


// ------------------------------------------------------------ DELETE ------------------------------------------------------------
router.delete('/:id', async (req, res) => {
  const user = await User.findByIdAndDelete(req.params.id)

  if(!user){
    return res.status(404).send("El usuario con ese ID no se encuentra registrado")
  }

  res.status(200).send("Ususario borrado")
})


module.exports = router