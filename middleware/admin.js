module.exports = function (req, res, next) {
  if(!req.user.isAdmin)
    return res.status(403).send('Acceso denegado') // 403 Json Web token valido pero ese recuros no esta permitido para ese usuario
  next()
}