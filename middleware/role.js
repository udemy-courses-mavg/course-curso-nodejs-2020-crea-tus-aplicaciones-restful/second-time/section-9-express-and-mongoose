function authorize(roles = []){
  if(typeof roles === 'string') {
    roles = [roles]
  }

  return [
    (req, res, next) => {
      if(!roles.includes(req.user.role))
        return res.status(403).send('Acceso Denegado, no tienes el rol permitido para acceder al recurso')
      next()
    }
  ]
}

module.exports = authorize